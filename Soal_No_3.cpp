#include <iostream>
#include <stack>

using namespace std;

bool sama(stack<int> s1, stack<int> s2) {
    if(s1.size() != s2.size()) {
        return false;
    }

    while(!s1.empty()) {
        if(s1.top() != s2.top()) {
            return false;
        }
        s1.pop();
        s2.pop();
    }

    return true;
}

int main() {
    stack<int> s1, s2;
    int n1, n2, temp;

    cout << "Masukkan ukuran stack 1: ";
    cin >> n1;

    for(int i=0; i<n1; i++) {
        cout << "Masukkan elemen " << i+1 << " dari stack 1: ";
        cin >> temp;
        s1.push(temp);
    }

    cout << "Masukkan ukuran stack 2: ";
    cin >> n2;

    for(int i=0; i<n2; i++) {
        cout << "Masukkan elemen " << i+1 << " dari stack 2: ";
        cin >> temp;
        s2.push(temp);
    }

    if(sama(s1, s2)) {
        cout << "Kedua stack memiliki nilai yang sama." << endl;
    } else {
        cout << "Kedua stack tidak memiliki nilai yang sama." << endl;
    }

    return 0;
}