#include<iostream>
#include<map>

using namespace std;

int main() {
    //Data Array
    map<string, int> menu = {
        {"Soto", 15000},
        {"Rawon", 20000},
        {"Pecel", 10000},
        {"Bakso", 12500},
        {"Siomay", 25000}
    };

    //Input pesanan
    cout << "===========================================" << endl;
    cout << " Soto   Rp 15000         Bakso   Rp 12500" << endl;
    cout << " Rawon  Rp 20000         Siomay  Rp 25000" << endl;
    cout << " Pecel  Rp 10000" << endl;
    cout << "===========================================" << endl;
    cout << "Isikan Makanan yang dipesan (Contoh : Soto 2 Pecel 4) : ";
    string input;
    getline(cin, input);

    //Proses
    map<string, int> pesanan;
    string masukan;
    size_t pos = 0;
    while ((pos = input.find(" ")) != string::npos) {
        masukan = input.substr(0, pos); 
        if (menu.count(masukan) == 1) { 
            input.erase(0, pos + 1); 
            if ((pos = input.find(" ")) != string::npos) { 
                int porsi = stoi(input.substr(0, pos)); 
                pesanan[masukan] = porsi; 
                input.erase(0, pos + 1); 
            }
            else { 
                int porsi = stoi(input); 
                pesanan[masukan] = porsi; 
                break; 
            }
        }
        else { 
            input.erase(0, pos + 1); 
        }
    }

    //Menghitung total
    int total = 0;
    for (auto it = pesanan.begin(); it != pesanan.end(); it++) {
        string nama = it->first;
        int harga = menu[nama];
        int porsi = it->second;
        int subtotal = harga * porsi;
        cout << nama << " @" << harga << " * " << porsi << " = " << subtotal << endl;
        total += subtotal;
    }

    cout << "Total = " << total << endl;

    return 0;
}